﻿Drupal 7 Basical module for JQuery Calendar widget (http://codecanyon.net/item/booking-calendar-pro-jquery-plugin/244645)

Put in the folder itp_booking JQuery Calendar plugin. Path must be such: itp_booking/dopbcp

Add new roles: seller and customer users.
Settings on a permission page rules for seller and customer users where seller role enable for seller user in the  itp_booking module. And for customer user too. And enable seller and customer users together.

Seller user must have opportunity add new offers.

For normal work module you must install jquery_update module and enable version 1.11 and enable jquery migrate.

In module file itp_booking.module in the function itp_booking_page_alter set you content type, where you want add JQuery Calendar library. 

For show Calendar you must write next code in you node template:
 // Show booking calendar.
 print '<div id="booking-calendar"></div>';
 
If you want keep in DB you settins and orders, place in the callback functions your code (see itp_booking.module).

Developer: Danilevsky Kirill, k.danilevsky@gmail.com, web: http://best-house.org
