/*
 *  View booking calendar.
 *
 * For show Calendar you must write next code in you node template:
 * // Show booking calendar.
 * print '<div id="booking-calendar"></div>';
 */

(function ($) {
    Drupal.behaviors.itp_booking = {
        attach: function (context, settings) {
            // Execute code only once.
            if (typeof(init_discount) == "undefined") {
                // Execute code always when you call Ajax.
                init_discount = true;
                var baseUrl = Drupal.settings.itp_booking.baseUrl;
                var type = Drupal.settings.itp_booking.type;
                var nid = Drupal.settings.itp_booking.nid;

                if (type == 'backend') {
                    $('#booking-calendar').DOPBackendBookingCalendarPRO({
                        'ID': nid,
                        'loadURL': baseUrl + '/calendar-booking-load/',
                        'saveURL': baseUrl + '/calendar-booking-save/'

                    });
                }
                else {
                    var data_front = {
                        'ID': nid,
                        'loadURL': baseUrl + '/calendar-booking-load',
                        'sendURL': baseUrl + '/calendar-booking-save'
                    };
                    // Show booking calendar frontend.
                    $('#booking-calendar').DOPFrontendBookingCalendarPRO(data_front);
                }
            }
        }
    };
})(jQuery);
